import numpy as np
import os
from keras import callbacks
from keras.models import load_model

from bin.lib.graph import *
from bin.lib.preprocess import *
from bin.lib.report import *

from bin.lib.housekeeping import *


def kerasTrainAI(*argvs, **kwargs):
    argv = list(argvs)
    if argv:
        sys.argv.extend(argv)

    X_Train, Y_Train, X_Validate, Y_Validate, Final_train_Df, Final_validate_Df = process_input(
        inputparams)

    activation_layers_list = kwargs.get("activation_layers_list", [])

    output_folder_path = inputparams.output_folder_path + "/%s_%s_%s" % (
        activation_layers_list[0].__name__, activation_layers_list[1].__name__, activation_layers_list[2].__name__)

    if not os.path.exists(output_folder_path):
        logging.error(
            "Output folder path do not exist, Creating a output Folder")
        os.makedirs(output_folder_path)

    log_folder_path = inputparams.log_folder_path + "/%s_%s_%s" % (
        activation_layers_list[0].__name__, activation_layers_list[1].__name__, activation_layers_list[2].__name__)

    if not os.path.exists(log_folder_path):
        logging.error("Log folder path do not exist, Creating a log Folder")
        os.makedirs(log_folder_path)

    initialize_logger(inputparams.log_folder_path)

    Y_Train_Dataframe = pd.DataFrame(Y_Train)
    Y_Validate_Dataframe = pd.DataFrame(Y_Validate)
    Train_Index_Dataframe = pd.DataFrame(X_Train.index, columns=['Index'])
    Validate_Index_Dataframe = pd.DataFrame(
        X_Validate.index, columns=['Index'])

    col = X_Train.shape[1]

    create_graph(activation_layers_list, col)
    optimizer_type = inputparams.optimizer_type
    learn_rate = inputparams.learning_rate
    opt = opt_type(optimizer_type, learn_rate, logging)

    model.compile(loss=c_loss, optimizer=opt, metrics=['mse'])

    # Add logs for TensorBoard Analysis
    TensorBoardCallBack = callbacks.TensorBoard(log_dir='%s/' % (log_folder_path), histogram_freq=0, write_graph=True,
                                                write_images=True)

    # Fit the model
    model.fit(X_Train, Y_Train, epochs=inputparams.training_epochs, batch_size=len(X_Train),
              callbacks=[TensorBoardCallBack])

    # Save Model to the disk with HDF5 format
    model.save('%s/store_model.h5' % (output_folder_path))

    model.save_weights('%s/model_weights.h5' % (output_folder_path))

    # Load Model from the disk.
    loaded_model = load_model(
        '%s/store_model.h5' % (output_folder_path), custom_objects={'c_loss': c_loss})

    # Evaluate the model and print Taining Accuracy
    Train_score = loaded_model.evaluate(X_Train, Y_Train)

    # Print the Training Accuracy on the Training Data
    print(("\n\nTraining Accuracy Matrix : :%s: %.2f%%" %
           (loaded_model.metrics_names[1], Train_score[1] * 100)))

    # Evaluate the model and print Validation Accuracy
    Validate_score = loaded_model.evaluate(X_Validate, Y_Validate)

    # Print the Validation Accuracy on the Validation Data
    print(("\nValidation Accuracy Matrix: %s: %.2f%%" %
           (loaded_model.metrics_names[1], Validate_score[1] * 100)))

    # Predict the values on basis of Input
    Train_Predictions = loaded_model.predict(X_Train)
    Validate_Predictions = loaded_model.predict(X_Validate)

    # Calculate diff and create Accuray Report
    ydiff_Train = []
    ydiff_Validate = []

    generate_csv(output_folder_path, Final_train_Df, Final_validate_Df)

    for x, y in zip(Y_Train.values, Train_Predictions):
        ydiff_Train.extend(np.subtract(x, y) / (0.01))
    generatesheets(Y_Train_Dataframe, Train_Predictions, ydiff_Train, 'Training',
                   Train_Index_Dataframe, output_folder_path)

    for x1, y1 in zip(Y_Validate.values, Validate_Predictions):
        ydiff_Validate.extend(np.subtract(x1, y1) / (0.01))

    generatesheets(Y_Validate_Dataframe, Validate_Predictions, ydiff_Validate, 'Validation',
                   Validate_Index_Dataframe, output_folder_path)


if __name__ == "__main__":
    kerasTrainAI()
