import sys

import os
from keras.models import load_model

from bin.lib.graph import *
from bin.lib.preprocessUseAI import *
from bin.lib.reportUseAI import generate_test_sheet
from bin.lib.housekeeping import *

messages, error = validateinput(inputparams)
if error:
    logging.error(messages)
    sys.exit(error)

X_Test = process_input(inputparams)
Test_Index = pd.DataFrame(X_Test.index, columns=['Index'])

col = X_Test.shape[1]
activation_layers_dict = {"1": tf.nn.sigmoid, "2": tf.nn.tanh, "3": tf.nn.relu}
activation_layers_list = [activation_layers_dict[str(inputparams.activation_function1)],
                          activation_layers_dict[str(
                              inputparams.activation_function2)],
                          activation_layers_dict[str(
                              inputparams.activation_function3)]
                          ]
output_folder_path = inputparams.output_folder_path + "/%s_%s_%s" % (
    activation_layers_list[0].__name__, activation_layers_list[1].__name__, activation_layers_list[2].__name__)
create_graph(activation_layers_list, col)
# Load Model from the disk.

try:
    model.load_weights('%s/model_weights.h5' % (output_folder_path))
    loaded_model = load_model(
        '%s/store_model.h5' % (output_folder_path), custom_objects={'c_loss': c_loss})
except Exception as e:
    logging.error("Error fetching stored model and weights ")
    logging.error("%s" % e)
    sys.exit(1)

# Predict the values on basis of Input
Train_Predictions = model.predict(X_Test)
Train_Predictions_Df = pd.DataFrame(Train_Predictions)
generate_test_sheet(Train_Predictions_Df, "Test_Predictions",
                    Test_Index, output_folder_path)
print("Predictions completed")
