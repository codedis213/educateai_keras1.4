import os
import tensorflow as tf
from EducateAIWithKeras import *


def automateActivation():
    activation_layers_dict = {"1": tf.nn.sigmoid,
                              "2": tf.nn.tanh, "3": tf.nn.relu}
    list_activation_functions = ["1", "2", "3"]
    messages, error = validateinput(inputparams)
    if error:
        logging.error(messages)
        sys.exit(error)
    if (inputparams.all_activation_layer == "1"):
        print("Inside multiple activation")
        for x in list_activation_functions:
            for y in list_activation_functions:
                for z in list_activation_functions:
                    activation_layers_list = [activation_layers_dict[z],
                                              activation_layers_dict[y],
                                              activation_layers_dict[x]
                                              ]
                    kerasTrainAI(activation_layers_list=activation_layers_list)
    else:

        activation_layers_dict = {"1": tf.nn.sigmoid,
                                  "2": tf.nn.tanh, "3": tf.nn.relu}
        activation_layers_list = [activation_layers_dict[str(inputparams.activation_function1)],
                                  activation_layers_dict[str(
                                      inputparams.activation_function2)],
                                  activation_layers_dict[str(
                                      inputparams.activation_function3)]
                                  ]
        kerasTrainAI(activation_layers_list=activation_layers_list)


if __name__ == "__main__":
    automateActivation()
