def generate_test_sheet(ytrain, acctype, index, output_folder_path):
    tempframe = index.copy()
    tempframe['AI_Result'] = ytrain
    tempframe.to_csv(path_or_buf="%s/%sReport.csv" %
                     (output_folder_path, acctype), index=False)
