import sys
import pandas as pd
from sklearn import preprocessing
from bin.lib.housekeeping import *


def process_input(inputparams):
    # Mapping of columns with the names
    TrainingColumns = inputparams.mapping_input_column.split(",")

    # Dividing Numerical and Categorical Columns
    Numerical = TrainingColumns[:]

    # Creating dataframes for training and result columns
    try:
        if not os.path.exists(inputparams.input_folder_path + "/" + inputparams.training_file_name):
            logging.error(
                "There is no input file specified , please add input file inside input folder for scaling validation data on same scale as training ")
            sys.exit()

        Training_Df = pd.read_csv(inputparams.input_folder_path + "/" + inputparams.training_file_name, delimiter=",",
                                  header=0,
                                  usecols=TrainingColumns)
        if not os.path.exists(inputparams.input_folder_path + "/" + inputparams.test_file_name):
            logging.error(
                "There is no test file specified , please add test file inside input folder")
            sys.exit()

        Test_Df = pd.read_csv(inputparams.input_folder_path + "/" + inputparams.test_file_name, delimiter=",", header=0,
                              usecols=TrainingColumns)
        # Creating Numerical and Categorical dataframe
        Numerical_Df_Training = Training_Df[Numerical]
        Numerical_Df_Testing = Test_Df[Numerical]

        # Preprocessing of numerical data
        scaler = preprocessing.StandardScaler(
            copy=True, with_mean=True, with_std=True).fit(Numerical_Df_Training)
        Preprocessed = scaler.transform(Numerical_Df_Testing)
        Preprocessed_Df = pd.DataFrame(Preprocessed, columns=Numerical)

        # Concat both numerical and categorical data for processing
        Final_Df = pd.concat([Preprocessed_Df], axis=1)

    except Exception as e:
        logging.error(
            "Error reading input data sheet, Please verify the format")
        logging.error("%s" % e)
        sys.exit(1)
    return Final_Df


def validateinput(inputparams):
    if inputparams.learning_rate <= 0:
        return str("Invalid Learning Rate , Learning Rate shpuld be in between 0 and 1"), int(1)
    elif inputparams.training_epochs <= 0:
        return str("Invalid Training Epochs Given, Training Epochs should be Positive"), int(1)
    elif inputparams.normalize_input not in [0, 1]:
        return str("Invalid value for normalize_input boolean expected, Either 0 or 1"), int(1)
    elif inputparams.trainTestRatio < 0 or inputparams.trainTestRatio > 1:
        return str("Invalid value for Test Data Ratio , Must be between 0 and 1"), int(1)
    elif inputparams.split not in [0, 1]:
        return str("Invalid value for Split, boolean expected, Either 0 or 1"), int(1)
    elif inputparams.shuffle not in [0, 1]:
        return str("Invalid value for shuffle boolean expected, Either 0 or 1"), int(1)
    elif inputparams.header_row_num < 0:
        return str("Invalid value for Header row number, Must be Positive"), int(1)
    elif inputparams.activation_function1 not in [1, 2, 3]:
        return str("Invalid value for Activation Function, Must be either 1, 2 or 3 "), int(1)
    elif inputparams.activation_function2 not in [1, 2, 3]:
        return str("Invalid value for Activation Function, Must be either 1, 2 or 3 "), int(1)
    elif inputparams.activation_function3 not in [1, 2, 3]:
        return str("Invalid value for Activation Function, Must be either 1, 2 or 3 "), int(1)
    elif inputparams.all_activation_layer not in [0, 1]:
        return str("Invalid value for All Activation layer , Must be either 1, 2 or 3 "), int(1)

    return "", 0


if __name__ == "__main__":
    process_input()
