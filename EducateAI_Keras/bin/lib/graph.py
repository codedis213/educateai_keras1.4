import os
import tensorflow as tf
from keras.layers import Dense
from keras.models import Sequential
from bin.lib.housekeeping import *

model = Sequential()


# Create model
def create_graph(activation_layers_list, col):
    activation1 = activation_layers_list[0]
    activation2 = activation_layers_list[1]
    activation3 = activation_layers_list[2]
    model.add(
        Dense(50, input_dim=col, use_bias=True, kernel_initializer='random_normal', bias_initializer='random_normal',
              activation=activation1, name='dense1'))
    model.add(Dense(50, use_bias=True, kernel_initializer='random_normal', bias_initializer='random_normal',
                    activation=activation2, name='dense2'))
    model.add(Dense(50, use_bias=True, kernel_initializer='random_normal', bias_initializer='random_normal',
                    activation=activation3, name='dense3'))
    model.add(Dense(1, use_bias=True, kernel_initializer='random_normal', bias_initializer='random_normal',
                    activation='sigmoid', name='dense4'))


# Calculate Loss
def c_loss(*args):
    y_true, y_pred = args
    reg_w1 = tf.nn.l2_loss(model.layers[0].get_weights()[0])
    reg_w2 = tf.nn.l2_loss(model.layers[1].get_weights()[0])
    reg_w3 = tf.nn.l2_loss(model.layers[2].get_weights()[0])
    reg_w4 = tf.nn.l2_loss(model.layers[3].get_weights()[0])
    return tf.reduce_mean(tf.nn.l2_loss(tf.subtract(y_pred, y_true))) + int(inputparams.reg_constant) * (
        reg_w1 + reg_w2 + reg_w3 + reg_w4)
