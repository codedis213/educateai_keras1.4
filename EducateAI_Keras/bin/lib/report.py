import sys

import pandas as pd


def generatesheets(iocpusection, ytrain, ydiff, acctype, index, output_folder_path):
    try:
        tempframe = index.copy()
        tempframe['Result'] = iocpusection
        tempframe['AI_Result'] = ytrain

        tempframe.to_csv(path_or_buf="%s/%sReport.csv" %
                         (output_folder_path, acctype), index=False)
        tempframe['Diff%'] = ydiff
        # Count if -10% <= diff  <= 10%
        totalcount = len(tempframe.index)
        diffcount = len(tempframe[(tempframe['Diff%'] <= 10) & (
            tempframe['Diff%'] >= -10)].index)
        ratio = (diffcount * 1.0 / totalcount)
        total_diff = totalcount - diffcount
        print(("\n%s Accuracy ratio: %s" % (acctype, ratio)))

        summtable = pd.DataFrame(data=[[totalcount, diffcount, total_diff, ratio]],
                                 columns=["Total", "Diff% value lies between +- 10%", "Diff% value not between +- 10%",
                                          "Ratio"])

        summtable.to_csv(path_or_buf="%s/%sAccuracyReport.csv" %
                         (output_folder_path, acctype), index=False)
        print(("Report generated and stored in %s " % (output_folder_path)))

    except Exception as e:
        log.error("Error occured while creating the educate model sheets: %s" % e)
        sys.exit(1)


def generate_csv(output_folder_path, Final_train_Df, Final_validate_Df):
    Final_train_Df.to_csv("%s/Shuffled_Training_Data.csv" %
                          (output_folder_path))
    Final_validate_Df.to_csv(
        "%s/Shuffled_Validation_Data.csv" % (output_folder_path))
