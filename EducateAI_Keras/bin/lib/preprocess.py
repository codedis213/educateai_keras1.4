import sys
import os
import pandas as pd
from keras import optimizers
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from bin.lib.housekeeping import *


def process_input(inputparams):
    # Mapping of columns with the names
    TrainingColumns = inputparams.mapping_input_column.split(",")

    ResultColumns = inputparams.data_column.split(",")

    # Dividing Numerical and Categorical Columns

    Numerical = TrainingColumns[:]

    input_folder_path = inputparams.input_folder_path
    if not os.path.exists(input_folder_path):
        logging.error(
            "There is no input folder path specified , please add input folder path in Configuration")
        sys.exit()

    # Creating dataframes for training and result columns
    if (int(inputparams.split) == 1):

        if not os.path.exists(input_folder_path + "/" + inputparams.training_file_name):
            logging.error(
                "There is no input file specified , please add input file inside input folder and specify in Configuration")
            sys.exit()
        try:

            Training_Df = pd.read_csv(inputparams.input_folder_path + "/" + inputparams.training_file_name,
                                      delimiter=",",
                                      header=0,
                                      usecols=TrainingColumns)
            Result_Df = pd.read_csv(inputparams.input_folder_path + "/" + inputparams.training_file_name, delimiter=",",
                                    header=0,
                                    usecols=ResultColumns)

            # Creating Numerical and Categorical dataframe
            Numerical_Df = Training_Df[Numerical]

            # Preprocessing of numerical data
            if (inputparams.normalize_input == 1):
                scaler = preprocessing.StandardScaler(
                    copy=True, with_mean=True, with_std=True).fit(Numerical_Df)
                Preprocessed = scaler.transform(Numerical_Df)
                Preprocessed_Df = pd.DataFrame(Preprocessed, columns=Numerical)
            else:
                Preprocessed_Df = Numerical_Df
            Final_Df = pd.concat([Preprocessed_Df], axis=1)

            if (int(inputparams.shuffle) == 1):
                X_train, X_test, Y_train, Y_test = train_test_split(Final_Df, Result_Df,
                                                                    test_size=float(
                                                                        inputparams.trainTestRatio),
                                                                    shuffle=True)

            else:
                X_train, X_test, Y_train, Y_test = train_test_split(Final_Df, Result_Df,
                                                                    test_size=inputparams.trainTestRatio, shuffle=False)
        except Exception as e:
            logging.error(
                "Error reading input training data sheet, Please verify the mapping columns name ")
            logging.error("%s" % e)
            sys.exit(1)

        Final_Training_Df = pd.concat([X_train, Y_train], axis=1)
        Final_Validate_Df = pd.concat([X_test, Y_test], axis=1)

    else:
        if not os.path.exists(input_folder_path + "/" + inputparams.training_file_name):
            logging.error(
                "Please specify both training file and validation file for this input,Add input Training file inside input folder and specify in Configuration")
            sys.exit()
        if not os.path.exists(input_folder_path + "/" + inputparams.validation_file_name):
            logging.error(
                "There is no input Validation file specified (manually give training and validation Files), please add input Validation file inside input folder and specify in Configuration")
            sys.exit()

        try:
            Training_Df = pd.read_csv(inputparams.input_folder_path + "/" + inputparams.training_file_name,
                                      delimiter=",",
                                      header=0)
            Validation_Df = pd.read_csv(inputparams.input_folder_path + "/" + inputparams.validation_file_name,
                                        delimiter=",", header=0)

            if (int(inputparams.shuffle) == 1):
                Training_Df = Training_Df.sample(frac=1)
                Validation_Df = Validation_Df.sample(frac=1)
            Training_Df_sub = Training_Df[TrainingColumns]
            Training_Result_Df = Training_Df[ResultColumns]
            Validation_Df_sub = Validation_Df[TrainingColumns]
            Validation_Result_Df = Validation_Df[ResultColumns]

            # Creating Numerical and Categorical dataframe
            Training_Numerical_Df = Training_Df_sub[Numerical]
            Validation_Numerical_Df = Validation_Df_sub[Numerical]
            if (inputparams.normalize_input == 1):
                scaler = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True).fit(
                    Training_Numerical_Df)
                Training_Preprocessed = scaler.transform(Training_Numerical_Df)
                Validation_Preprocessed = scaler.transform(
                    Validation_Numerical_Df)
                Training_Preprocessed_Df = pd.DataFrame(
                    Training_Preprocessed, columns=Numerical)
                Validation_Preprocessed_Df = pd.DataFrame(
                    Validation_Preprocessed, columns=Numerical)
            else:
                Training_Preprocessed_Df = Training_Numerical_Df
                Validation_Preprocessed_Df = Validation_Numerical_Df
            Training_Final_Df = pd.concat([Training_Preprocessed_Df], axis=1)
            Validation_Final_Df = pd.concat(
                [Validation_Preprocessed_Df], axis=1)

            X_train = Training_Final_Df
            X_test = Validation_Final_Df
            Y_train = Training_Result_Df
            Y_test = Validation_Result_Df

            Final_Training_Df = pd.concat([X_train, Y_train], axis=1)
            Final_Validate_Df = pd.concat([X_test, Y_test], axis=1)

        except Exception as e:
            logging.error(
                "Error reading input training data sheet, Please verify the mapping columns name ")
            logging.error("%s" % e)
            sys.exit(1)
    return X_train, Y_train, X_test, Y_test, Final_Training_Df, Final_Validate_Df


def opt_type(optimizer_type, learn_rate, log):
    if optimizer_type == 1:
        opt = optimizers.Adam(lr=learn_rate)
    elif optimizer_type == 2:
        opt = optimizers.SGD(lr=learn_rate)
    elif optimizer_type == 3:
        opt = optimizers.Adadelta(lr=learn_rate)
    elif optimizer_type == 4:
        opt = optimizers.Adagrad(lr=learn_rate)
    elif optimizer_type == 5:
        opt = optimizers.Nadam(lr=learn_rate)
    elif optimizer_type == 6:
        opt = optimizers.RMSprop(lr=learn_rate)
    else:
        log.error("Optimizer type not supported( use either 1 to 6)")
        sys.exit(2)
    return opt


if __name__ == "__main__":
    process_input()


def validateinput(inputparams):
    if inputparams.learning_rate <= 0 or inputparams.learning_rate > 1:
        return str("Invalid Learning Rate , Learning Rate shpuld be in between 0 and 1"), int(1)
    elif inputparams.training_epochs <= 0:
        return str("Invalid Training Epochs Given, Training Epochs should be Positive"), int(1)
    elif inputparams.normalize_input not in [0, 1]:
        return str("Invalid value for normalize_input boolean expected, Either 0 or 1"), int(1)
    elif inputparams.trainTestRatio < 0 or inputparams.trainTestRatio > 1:
        return str("Invalid value for Test Data Ratio , Must be between 0 and 1"), int(1)
    elif inputparams.split not in [0, 1]:
        return str("Invalid value for Split, boolean expected, Either 0 or 1"), int(1)
    elif inputparams.shuffle not in [0, 1]:
        return str("Invalid value for shuffle boolean expected, Either 0 or 1"), int(1)
    elif inputparams.header_row_num < 0:
        return str("Invalid value for Header row number, Must be Positive"), int(1)
    elif inputparams.activation_function1 not in [1, 2, 3]:
        return str("Invalid value for Activation Function, Must be either 1, 2 or 3 "), int(1)
    elif inputparams.activation_function2 not in [1, 2, 3]:
        return str("Invalid value for Activation Function, Must be either 1, 2 or 3 "), int(1)
    elif inputparams.activation_function3 not in [1, 2, 3]:
        return str("Invalid value for Activation Function, Must be either 1, 2 or 3 "), int(1)
    elif inputparams.all_activation_layer not in [0, 1]:
        return str("Invalid value for All Activation layer , Must be either 1, 2 or 3 "), int(1)

    return "", 0
