# This module does some of the housekeeping work like initializing logger etc.

from datetime import datetime
import sys
import configargparse
import logging
import os
from os.path import expanduser

LOGGER = logging.getLogger("EducateAI_Keras")

program_name = os.path.dirname(__file__)
program_path = os.path.realpath(program_name)


def initialize_config_parser(program_path):
    """Initializing configuration"""

    try:
        system_config_path_temp = "../../conf/SysDesignGen.conf"
        system_config_path = os.path.abspath(
            program_path + "/" + system_config_path_temp)

        is_config_path = False

        if "-c" in sys.argv:
            cindex = sys.argv.index("-c")
            is_config_path = True
        elif "--my-config" in sys.argv:
            cindex = sys.argv.index("--my-config")
            is_config_path = True

        if is_config_path:
            system_config_path_temp2 = sys.argv[cindex+1]
            if os.path.isabs(system_config_path_temp2) and os.path.isfile(system_config_path_temp2):
                system_config_path_temp = system_config_path_temp2
                system_config_path = system_config_path_temp
            else:
                logging.error(
                    "%s, is not a absoulte path or File not found at absoulte path" % system_config_path_temp2)
                logging.error(
                    "please provide the absoult path like: %s/../../conf/SysDesignGen.conf" % program_path)
                sys.exit(2)

        print(("Configuration path: %s" % system_config_path))
        home_config_temp = expanduser("~/.SysDesignGen/SysDesignGen.conf")
        home_config_path = os.path.abspath(home_config_temp)

        # print absConfigPath

        args = configargparse.ArgParser(
            default_config_files=[system_config_path, home_config_path])
        args.add('-c', '--my-config', required=False,
                 is_config_file=True, help='config file path')
        args.add('-i', '--input_folder_path', required=True,
                 help='Input folder path of input files')
        args.add('-o', '--output_folder_path', required=True,
                 help='Output folder path of output files shall be saved')
        args.add('-l', '--log_folder_path', required=True,
                 help='Log folder path where log files would be created')
        args.add('-mp', '--mapping_input_column',
                 required=True, help='Mapping input column')
        args.add('-dc', '--data_column', required=False, help='Data column')
        args.add('-tf', '--training_file_name',
                 required=True, help='Input Training file')
        args.add('-vf', '--validation_file_name',
                 required=False, help='Input Validation file')
        args.add('-hd', '--header_row_num', required=True,
                 type=int, help='Header Row No')
        args.add('--index_column', required=True,
                 help='Name of the column with scenario Ids')
        args.add('--learning_rate', type=float,
                 required=True, help='learning rate')
        args.add('--training_epochs', type=int,
                 required=True, help='training_epochs')
        args.add('--normalize_input', type=int,
                 required=True, help='normalize_input')
        args.add('--optimizer_type', type=int,
                 required=True, help='optimizer_type')
        args.add('--trainTestRatio', type=float,
                 required=True, help='trainTestRatio')
        args.add('--reg_constant', type=float,
                 required=True, help='reg_constant')
        args.add('--AP_exclude', required=False, help='ap_exclude')
        args.add('--DBMS_exclude', required=False, help='dbms_exclude')
        args.add('--SA_exclude', required=False, help='sa_exclude')
        args.add('--WEB_exclude', required=False, help='web_exclude')
        args.add('--MQ_exclude', required=False, help='mq_exclude')
        args.add('--exclude', type=int, required=False, help='exclude')
        args.add('--test_file_name', required=False, help='Test file')
        args.add('--activation_function1', type=int,
                 required=False, help='Activation function1')
        args.add('--activation_function2', type=int,
                 required=False, help='Activation function 2')
        args.add('--activation_function3', type=int,
                 required=False, help='Activation function 3')
        args.add('--all_activation_layer', type=int, required=False,
                 help='Combination of all Activation function')
        args.add('--split', required=False, type=int,
                 help='For split Train Test File via Code')
        args.add('--shuffle', required=False, type=int,
                 help='Whether to shuffle training data or not')
        args.add('--categorical_columns', required=False,
                 help='specify the categorical columns in the data')

    except Exception as e_temp:

        logging.debug("Error while initialize Configuration parser: ", e_temp)

    return args


def initialize_logger(logpath=""):
    """initialize logger create logger"""

    if not os.path.exists(logpath):
        os.makedirs(logpath)

    LOGGER.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    # create file handler and set level to debug
    file_handler = logging.FileHandler(
        logpath + "EducateAI_Keras" + "_" + datetime.now().strftime("%Y%m%d-%H%M%S") + ".log")

    file_handler.setLevel(logging.DEBUG)

    # add formatter to ch
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    file_handler.setFormatter(logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))

    # add ch to logger

    LOGGER.addHandler(console_handler)
    LOGGER.addHandler(file_handler)

    return 0


args = initialize_config_parser(program_path)
inputparams = args.parse_args()
