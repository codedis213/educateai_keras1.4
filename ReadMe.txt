﻿[Script Version] : 1.3
[Script Name]	 : EducateAI_Keras
[Script Updated] : 22-03-2018
[ReadMe Version] : 1.3
[ReadMe Updated] : 22-03-2018


File Structure:
EducateAI_Keras
├── bin
│   ├── EducateAI_Keras.py
│   ├── EducateAIWithKeras.py
│   ├── __init__.py
│   ├── lib
│   │   ├── graph.py
│   │   ├── housekeeping.py
│   │   ├── __init__.py
│   │   ├── preprocess.py
│   │   ├── preprocessUseAI.py
│   │   ├── report.py
│   │   ├── reportUseAI.py
│   └── useAI.py
├── conf
│   └── SysDesignGen.conf
├── input
│   ├── DBMS_Training_Data.csv
│   ├── SA_Training_Data.csv
├── logs
└── output
    └── tanh_sigmoid_sigmoid
        ├── model_weights.h5
        ├── Shuffled_Training_Data.csv
        ├── Shuffled_Validation_Data.csv
        ├── store_model.h5
        ├── Test_PredictionsReport.csv
        ├── TrainingAccuracyReport.csv
        ├── TrainingReport.csv
        ├── ValidationAccuracyReport.csv
        └── ValidationReport.csv

		
Training_Data
	+--	DBMS_Training_Data.csv
	+--	SA_Training_Data.csv
		
AMI and Instance Details:

1. Tensorflow AMI			(ami-74a88711) 				: Refer Row No.10 of AWS AMIs sheet of the aws inventory
2. Tensorflow Demo instance	(i-0a4b773af2f0370be)		: IP : 172.31.34.238
2. Tensorboard Instance 	(i-007bd12172f05301b) 		: Refer Row No.3 of the aws EC2 instance sheet of aws inventory list provided.
3. Windows EC2 instance 	(i-02d942a48e799ec42)		: Refer Row No.5 of the aws EC2 instance sheet of aws inventory list provided.

Execution Environment: Linux EC2 instance (p2.xlarge with gpu)
Operating System : CentOS Linux release 7.2.1511 (Core)
NAME="CentOS Linux"
VERSION="7 (Core)"

Python Version: 2.7

Install the Libraries used with the following command :    
														sudo yum install python2-pip 
														sudo pip install tensorflow
														sudo pip install pandas
														sudo yum install numpy
														sudo pip install sklearn
														sudo pip install configargparse
														sudo pip install Keras
														sudo pip install h5py

Execution Steps [educateAI script]:

1.  Refer the above mention tensorflow AMI and create a tensorflow instance with following specifications:
	a. EC2 gpu instance
	b. type - p2.xlarge

	OR

	Use the tensorflow demo instance created by NTI with
	instance ID: i-0a4b773af2f0370be
	IP Address: 172.31.34.238

2.  Login to the instance.

3. 	Copy EducateAI_Keras folder at the tensorflow instance at Location :/home/centos/

4.  Copy the Files inside Training_Data Folder to the directory educateAI/input/
	(Note: For ease of NEC with this Training data , NTI has already added the Files in TrainingData folder to the educateAI/input directory).

5. 	Move to the educateAI directory with the following command:
		Command:
			cd EducateAI_Keras/
6. 	Open configuration file:
	Command:
		vi SysDesignGen.conf

7.  Edit the Configuration file for following parameters if already not set:
		a. log_folder_path		: Absolute path of the log folder inside 'educateAI/' directory.		
		b. input_folder_path	: Absolute path of the input folder inside 'educateAI/' directory.	
		c. output_folder_path	: Absolute path of the output folder inside 'educateAI/' directory.	
		d. training_file_name	: Training file name with extension	
		e. validation_file_name	: Validation Fle name with extension.
		e. test_file_name		: Test file name with extension.
		f. shuffle				: For enabling/Disabling the option of shuffling the training and validation data
		g. split				: Parameter for giving only one file(Input csv) as input OR 2 files as input(training csv file and validation csv file) 

8.  Edit the configuration file for following NN parameters:
		[Neural Network Parameters]
		a. learning_rate		:  Learning rate for the training		
		b. training_epochs		:  No. of times training has to be done on same set of data
		c. normalize_input		:  Binary value(Normalise Input:1, Do no Normalise Input: 0)
		d. optimizer_type		:  type of optamizer to use , Range[1,10] 
		e. reg_constant			:  Regularisation factor for the script
		
9.	Edit the configuration file for following parameters:
		1. To run the Educate AI Script for all possible combination of activation function at each layer.
			a. set "all_activation_layer" to 1

		2. To run the Educate AI for a only one combination of activation function at each layer
			a. set "all_activation_layer" to 0
			b. set the parameter activation_function1, activation_function2 and activation_function3 from the option 1 or 2 or 3 
		   	   where 1 is for Sigmoid, 2 for Tanh and 3 for Relue
                   	example:
                        	all_activation_layer = 0
				activation_function1 = 3 
				activation_function2 = 1
				activation_function3 = 1


10.	Edit the Configuration file for
		a. mapping_input_column : To map the feature columns from training data prepared.
		b. data_column			: To map the label column from the training data prepared.
		c. index_column			: to map the index column from the training data prepared
		d. categorical_columns	: To map the categorical columns for encoding the data
	
	For eg. While mapping in configuration file,
	
	a. index_column : use first column of training data i.e. h_0
	index_column=h_0

	b. mapping_input_column : use columns h_1 to second last column in training data
	e.g. for SA_Training_Data ( sent by NEC on Jan/18/2018)
	mapping_input_column=h_1,h_2,h_3,h_4,h_5,h_6,h_7,h_8, h_9, h_10
        where 
		h_1:  Service_down_time	
		h_2:  Business_Continuity_requirement_grade	
		h_3:  online_response_time
		h_4:  online_response_time_keep_ratio	
		h:5:  batch duration	
		h_6:  normal_throughput_requirement	
		h_7:  peak_throughput_requirement	
		h_8:  batch_amount	
		h_9:  Budget	
		h_10: SystemArchitecture


	c. data_column	: use last column of training data file
	e.g. for SA_Training_Data ( sent by NEC on Jan/18/2018)
	data_column=h_11
	where h_11 represents Result column 

11. To set the PYTHONPATH to the absolute path of educateAI Folder
	Run Command :
			 export PYTHONPATH=/home/centos/EducateAI_Keras/

12. To execute the educateAI source code for Training and Validation
	1. To run the Educate AI Script for all possible combination of activation function at each layer.
		a. set "all_activation_layer" to 1

	2. To run the Educate AI for a only one combination of activation function at each layer
		a. set "all_activation_layer" to 0
		b. set the parameter activation_function1, activation_function2 and activation_function3 from the option 1 or 2 or 3 
		   where 1 is for Sigmoid, 2 for Tanh and 3 for Relue
            example:
            all_activation_layer = 0
			activation_function1 = 3 
			activation_function2 = 1
			activation_function3 = 1
			
	3. Run Command :
			 python EducateAI_Keras.py


13.	Execution with single training file or 2 different files(training and validation) 
	1.To Run the training with only one input file :
	  set Configuration:
						split=1
						shuffle =1
						all_activation_layer = 0
	Explaination: When user want the script to automatically split the file in training data and validation data use the avove mentioned configuration.
	Note : It is mandatory to properly specify "training_file_name" parameter in the configuration file.
	
	2.To Run the training with seperate training and validation file :
	  set Configuration:
						split=0
						all_activation_layer = 0
	Explaination: when user manually inputs training data file and validation data file.
	Note : It is mandatory to properly specify "training_file_name" and "validation_file_name" parameter in the configuration file. 

14. Output File Structure:
		output
				+-- store_model.h5				: The trained model file
				+-- model_weights.h5			: Stored weights of the model.
				+-- TrainingReport.csv			: Report generated to comapare the Given Result and AI generated Result.
				+--	Shuffled_Training_Data.csv	: Actual Training Data used for testing after shuffling the data.
				+-- TrainingAccuracyReport		: Report generated to check the Training Accuracy.
				+-- ValidationReport.csv		: Report generated to comapare the Given Result and AI generated Result.
				+--	Shuffled_Validation_Data.csv: Actual Validation Data used for testing after shuffling the data.
				+-- ValidationAccuracyReport.csv: Report generated to check the Validation Accuracy.
				+--	Test_PredictionsReport.csv	: Report generated to comapare the Given Result and AI generated Result.
	
			
15.Copy the report(TrainingReport.csv, TrainingAccuracyReport.csv,ValidationReport.csv,ValidationAccuracyReport.csv) to a local system for verification.

16. Copy the educateAI/logs/ folder to the tensorboard instance.

17. For Tuning the Model:
	a. Model can be improved by Tuning the parameters in configuration file.
	   Parameters: 	learning_rate
					training_epochs
					reg_constant
	
*************************************************************************************************************************************************************
= USE AI =
 
1. To execute the educateAI source code for Using the stored model(.h5)
    This execution is use to use the generated model(ckpt) or trained model.
    Please follow the steps below:

    1. set the parameter "test_file_name" to the test data file.
    2. set the parameter "all_activation_layer" to 0
    3. set activation_function1, activation_function2 and activation_function3 from the option 1, 2 and 3
       where 1 :sigmoid
			 2 :tanh
			 3 :relu
       for example 
			activation_function1 = 2 
			activation_function2 = 1
			activation_function3 = 1

    4. then Run Command :
			 python useAI.py
	5. The output will be generated in directory "output/"
	   Output will consist of prediction of each data row and corresponding index values.

    Note: Make sure to put a test file in input folder with same number of feature columns at the time of training.
	for eg. Test file should have the same format as training/Validation File and same number of feature columns.




19. Steps for execution on TensorBoard:

1.	Please start tensorboard private AWS instance (i-007bd12172f05301b).

2.	Please login into Windows EC2 instance (i-02d942a48e799ec42) first.

3.	Please open cygwin console and connect to tensorboard private AWS instance (i-007bd12172f05301b) using keypair (ntil-ond16).

4.	Run the following command:
	a.	tensorboard  --logdir=<location>/tf_eventsummary -port 6606
	b.	The <location> is the locations of logs copied on tensorboard instance in step 12 above.

5.	Once the above command successfully run, it will display the URL - http://172.31.8.209:6606 

6.	Use the Windows EC2 instance (i-02d942a48e799ec42) and Run IE and Access the url - http://172.31.8.209:6606

